const express = require('express')
const db = require('./db')
const cors = require('cors')

const router = express()


router.use(cors('*'))

router.use(express.json())

//Blank api for test (list of all movies)
router.get('/all', (request, response) => {
    const statement = `SELECT * FROM Movie`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})


//1. GET  --> Display Movie using name from Containerized MySQL
router.get('/:movie_title', (request, response) => {
    const { movie_title } = request.params

    const statement = `SELECT * FROM Movie WHERE movie_title = '${movie_title}'`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})

//2. POST --> ADD Movie data into Containerized MySQL table
//Movie(movie_id, movie_title, movie_release_date,movie_time,director_name)
router.post('/add_movie', (request, response) => {
    const { movie_id, movie_title, movie_release_date, movie_time, director_name } = request.body

    const statement = `INSERT INTO Movie (movie_id, movie_title, movie_release_date, movie_time, director_name)
                        VALUES ( ${movie_id}, '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})

//3. UPDATE --> Update Release_Date and Movie_Time into Containerized MySQL table
router.put('/:movie_id', (request, response) => {
    const { movie_id } = request.params
    const { movie_release_date, movie_time } = request.body
    //check whether AND is correct or comma should come instead of AND
    const statement = `UPDATE Movie SET movie_release_date = '${movie_release_date}',movie_time = '${movie_time}'
                          WHERE movie_id = ${movie_id} `
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})

//4. DELETE --> Delete Movie from Containerized MySQL (by Name)
router.delete('/:movie_title', (request, response) => {
    const { movie_title } = request.params

    const statement = `DELETE FROM Movie WHERE movie_title = '${ movie_title }'`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})


//4. DELETE --> Delete Movie from Containerized MySQL (by Id)
router.delete('/:movie_id', (request, response) => {
    const { movie_id } = request.params

    const statement = `DELETE FROM Movie WHERE movie_id = ${ movie_id }`
    const connection = db.openConnection()
    connection.query(statement, (error, result) => {
        connection.end()
        if (error) {
            response.send(error)
        } else {
            response.send(result)
        }
    })
})


router.listen(4000, () => {
    console.log('server started on port 4000')
})
